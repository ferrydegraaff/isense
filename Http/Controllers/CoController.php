<?php

namespace App\Http\Controllers;

use App\Co_settings;
use Illuminate\Http\Request;

class CoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){

        $coSettings  = Co_settings::all();

        return response()->json($coSettings);

    }

    public function getSettings($id){

        $coSettings  = Co_settings::find($id);

        return response()->json($coSettings);
    }

    public function saveSettings(Request $request){

        $coSettings = Co_settings::create($request->all());

        return response()->json($coSettings);

    }

    public function deleteSettings($id){
        $coSettings  = Co_settings::find($id);

        $coSettings->delete();

        return response()->json('success');
    }

    public function updateSettings(Request $request,$id){
        $coSettings  = Co_settings::find($id);

        $coSettings->CO_trigger_interval = $request->input('CO_trigger_interval');
        $coSettings->CO_fan_speed_on_trigger = $request->input('CO_fan_speed_on_trigger');
        $coSettings->CO_warning_message = $request->input('CO_warning_message');
        $coSettings->CO_threshold = $request->input('CO_threshold');



        $coSettings->save();

        return response()->json($coSettings);
    }
}

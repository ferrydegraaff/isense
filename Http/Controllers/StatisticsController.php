<?php

namespace App\Http\Controllers;

use App\Statistics;
use Illuminate\Http\Request;

class StatisticsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){

        $statistics  = Statistics::all();

        return response()->json($statistics);

    }

    public function getSettings($id){

        $statistics  = Statistics::find($id);

        return response()->json($statistics);
    }

    public function saveSettings(Request $request){

        $statistics = Statistics::create($request->all());

        return response()->json($statistics);

    }

    public function deleteSettings($id){
        $statistics  = Statistics::find($id);

        $statistics->delete();

        return response()->json('success');
    }

    public function updateSettings(Request $request,$id){
        $statistics  = Statistics::find($id);

        $statistics->title = $request->input('title');
        $statistics->content = $request->input('content');

        $statistics->save();

        return response()->json($statistics);
    }
}

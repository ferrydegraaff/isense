<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Co_settings extends Model
{
    protected $primaryKey = 'CO_settings_id';
    
    public $timestamps = false;

    protected $fillable = ['CO_trigger_value', 'CO_fan_speed_on_trigger', 'CO_warning_message'];
}

<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Fan extends Model
{
    protected $primaryKey = 'Fan_id';

    protected $table = "fan";
    
    public $timestamps = false;

    protected $fillable = ['Fan_speed', 'Fan_active'];
}

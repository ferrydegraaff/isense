<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Humidity_settings extends Model
{
    protected $primaryKey = 'Humidity_settings_id';
    
    public $timestamps = false;

    protected $fillable = ['Humidity_trigger_value', 'Humidity_fan_speed_on_trigger', 'Humidity_warning_message'];
}

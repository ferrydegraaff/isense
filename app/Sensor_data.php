<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Sensor_data extends Model
{
    protected $primaryKey = 'Sensor_data_id';

    protected $table = "sensor_data";
    
    public $timestamps = false;

    protected $fillable = ['Humidity_level', 'CO_level', 'Temp_level', 'Timestamp', 'UserUser_id'];
}

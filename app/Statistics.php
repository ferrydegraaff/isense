<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Statistics extends Model
{
    protected $primaryKey = 'Statistics_id';
    
    public $timestamps = false;

    protected $fillable = ['Type', 'Month', 'Median', 'Mode', 'Mean', 'UserUser_id'];
}

<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User_settings extends Model
{
    protected $primaryKey = 'User_settings_id';
    
    public $timestamps = false;

    protected $fillable = ['User_user_id', 'Co_settings_co_settings_id', 'Humidity_settings_humidity_settings_id', 'Parsing_interval'];
}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

header('Access-Control-Allow-Origin: *');

// CO settings
$app->get('api/settings/co', 'CoController@index');
$app->get('api/settings/co/{id}', 'CoController@getSettings');
$app->post('api/settings/co/{id}', 'CoController@updateSettings');
// $app->put('api/settings/co/{id}', 'CoController@updateSettings');
$app->delete('api/settings/co/{id}', 'CoController@deleteSettings');

// Humidity settings
$app->get('api/settings/humidity', 'HumidityController@index');
$app->get('api/settings/humidity/{id}', 'HumidityController@getSettings');
$app->post('api/settings/humidity/{id}', 'HumidityController@updateSettings');
// $app->put('api/settings/humidity/{id}', 'HumidityController@updateSettings');
$app->delete('api/settings/humidity/{id}', 'HumidityController@deleteSettings');

// Fan
$app->get('api/settings/fan', 'FanController@index');
$app->get('api/settings/fan/{id}', 'FanController@getSettings');
$app->post('api/settings/fan/{id}', 'FanController@updateSettings');
//$app->put('api/fan/{id}', 'FanController@updateSettings');
$app->delete('api/settings/fan/{id}', 'FanController@deleteSettings');

// Sensor data
$app->get('api/sensor_data', 'SensorController@index');
$app->get('api/sensor_data/amount/{limit}', 'SensorController@getSettingsTimeLimit');
$app->get('api/sensor_data/{id}', 'SensorController@getSettings');
$app->post('api/sensor_data', 'SensorController@saveSettings');
$app->put('api/sensor_data/{id}', 'SensorController@updateSettings');
$app->delete('api/sensor_data/{id}', 'SensorController@deleteSettings');

// Statistics
$app->get('api/statistics', 'StatisticsController@index');
$app->get('api/statistics/{id}', 'StatisticsController@getSettings');
$app->post('api/statistics', 'StatisticsController@saveSettings');
$app->put('api/statistics/{id}', 'StatisticsController@updateSettings');
$app->delete('api/statistics/{id}', 'StatisticsController@deleteSettings');

// User
$app->get('api/user', 'UserController@index');
$app->get('api/user/{id}', 'UserController@getSettings');
$app->post('api/user/{id}', 'UserController@updateSettings');
//$app->put('api/user/{id}', 'UserController@updateSettings');
$app->delete('api/user/{id}', 'UserController@deleteSettings');
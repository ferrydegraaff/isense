<?php

namespace App\Http\Controllers;

use App\Sensor_data;
use Illuminate\Http\Request;

class SensorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){

        $sensorData  = Sensor_data::all();

        return response()->json($sensorData);

    }
    
    public function getSettingsTimeLimit($limit){
        $sensorData = Sensor_data::orderBy('Sensor_data_id', 'desc')->take($limit)->get();
        
        return response()->json($sensorData);
    }

    public function getSettings($id){

        $sensorData  = Sensor_data::find($id);

        return response()->json($sensorData);
    }

    public function saveSettings(Request $request){

        $sensorData = Sensor_data::create($request->all());

        return response()->json($sensorData);

    }

    public function deleteSettings($id){
        $sensorData  = Sensor_data::find($id);

        $sensorData->delete();

        return response()->json('success');
    }

    public function updateSettings(Request $request,$id){
        $sensorData  = Sensor_data::find($id);

        $sensorData->title = $request->input('Hu');
        $sensorData->content = $request->input('content');

        $sensorData->save();

        return response()->json($sensorData);
    }
}

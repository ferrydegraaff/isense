<?php

namespace App\Http\Controllers;

use App\User;
use App\User_settings;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){

        $user  = User::all();

        return response()->json($user);

    }

    public function getSettings($id){
        $user  = User::find($id);
        $userSettings = User_settings::where('User_user_id', $id)->first();
        
        $user->parsing_interval = $userSettings->Parsing_interval;

        return response()->json($user);
    }

    public function saveSettings(Request $request){

        $user = User::create($request->all());

        return response()->json($user);

    }

    public function deleteSettings($id){
        $user  = User::find($id);

        $user->delete();

        return response()->json('success');
    }

    public function updateSettings(Request $request,$id){
        $user  = User::find($id);
        $userSettings = User_settings::where('User_user_id', $id)->first();
        
        if($request->input('Data_gathering') == "on"){
            $user->Data_gathering = 1;
        } else {
            $user->Data_gathering = 0;
        }
        
        $userSettings->Parsing_interval = $request->input('Parsing_interval');

        $user->save();
        $userSettings->save();

        return response()->json($user . $userSettings);
    }
}

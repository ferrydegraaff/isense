<?php

namespace App\Http\Controllers;

use App\Fan;
use Illuminate\Http\Request;

class FanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){

        $fan  = Fan::all();

        return response()->json($fan);

    }

    public function getSettings($id){

        $fan  = Fan::find($id);

        return response()->json($fan);
    }

    public function saveSettings(Request $request){

        $fan = Fan::create($request->all());

        return response()->json($fan);

    }

    public function deleteSettings($id){
        $fan  = Fan::find($id);

        $fan->delete();

        return response()->json('success');
    }

    public function updateSettings(Request $request,$id){
        $fan  = Fan::find($id);
        
        if (count($fan) == 0){
            $fan = new Fan();
        }
        
        if($request->input('Fan_active') == "on"){
            $fan->Fan_active = 1;
        } else {
            $fan->Fan_active = 0;
        }

        $fan->Fan_speed = $request->input('Fan_speed');

        $fan->save();

        return response()->json($fan);
    }
}

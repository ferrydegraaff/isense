<?php

namespace App\Http\Controllers;

use App\Fan;
use Illuminate\Http\Request;

class FanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){

        $fan  = Fan::all();

        return response()->json($fan);

    }

    public function getSettings($id){

        $fan  = Fan::find($id);

        return response()->json($fan);
    }

    public function saveSettings(Request $request){

        $fan = Fan::create($request->all());

        return response()->json($fan);

    }

    public function deleteSettings($id){
        $fan  = Fan::find($id);

        $fan->delete();

        return response()->json('success');
    }

    public function updateSettings(Request $request,$id){
        $fan  = Fan::find($id);

        $fan->Fan_speed = $request->input('Fan_speed');
        $fan->Fan_active = $request->input('Fan_active');

        $fan->save();

        return response()->json($fan);
    }
}

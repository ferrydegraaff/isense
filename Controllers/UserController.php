<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){

        $user  = User::all();

        return response()->json($user);

    }

    public function getSettings($id){

        $user  = User::find($id);

        return response()->json($user);
    }

    public function saveSettings(Request $request){

        $user = User::create($request->all());

        return response()->json($user);

    }

    public function deleteSettings($id){
        $user  = User::find($id);

        $user->delete();

        return response()->json('success');
    }

    public function updateSettings(Request $request,$id){
        $user  = User::find($id);

        $user->data_gathering = $request->input('data_gathering');

        $user->save();

        return response()->json($user);
    }
}

<?php

namespace App\Http\Controllers;

use App\Humidity_settings;
use Illuminate\Http\Request;

class HumidityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(){

        $humiditySettings  = Humidity_settings::all();

        return response()->json($humiditySettings);

    }

    public function getSettings($id){

        $humiditySettings  = Humidity_settings::find($id);

        return response()->json($humiditySettings);
    }

    public function saveSettings(Request $request){

        $humiditySettings = Humidity_settings::create($request->all());

        return response()->json($humiditySettings);

    }

    public function deleteSettings($id){
        $humiditySettings  = Humidity_settings::find($id);

        $humiditySettings->delete();

        return response()->json('success');
    }

    public function updateSettings(Request $request,$id){
        $humiditySettings  = Humidity_settings::find($id);

        $humiditySettings->Humidity_trigger_interval = $request->input('Humidity_trigger_interval');
        $humiditySettings->Humidity_fan_speed_on_trigger = $request->input('Humidity_fan_speed_on_trigger');
        $humiditySettings->Humidity_warning_message = $request->input('Humidity_warning_message');
        $humiditySettings->Humidity_threshold = $request->input('Humidity_threshold');

        $humiditySettings->save();

        return response()->json($humiditySettings);
    }
}
